# vaccine-notifier

Uses the API from https://www.vaccinespotter.org to watch for new appointments for a given address + distance combination, then uses AppleScript and Messages to send a text notification with links for each provider who has open appointments.

## Requirements
- Python 3.x
- MacOS
- Messages configured to send SMS

## Usage

- `pip install -r requirements.txt`
- See configuration options: `./watch.py --help`
- Basic operation: `./watch.py`
  - Will ask for address and phone number input.
- With address and phone number:
  - `./watch.py --address '95758' --phone-number '14087771234'`
  - `./watch.py --address '4000 Main Street, Los Angeles, CA' --phone-number '14087771234'`
