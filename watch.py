#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict, Optional, Set, Tuple
from geopy.distance import geodesic
from geopy.geocoders import Nominatim
from geopy import Location
import requests
from cachecontrol import CacheControl
from cachecontrol.caches.file_cache import FileCache
from pprint import pprint
import click
from dataclasses import dataclass
from os import path
from subprocess import Popen, PIPE
import pickle
from datetime import datetime as dt, timedelta
import urllib
from dateutil import parser
import time



CONFIG_PKL = path.join(path.dirname(__file__), 'appts.pkl')
NOTIFICATION_PKL = path.join(path.dirname(__file__), 'notifications.pkl')


sess = CacheControl(requests.Session(),
                    cache=FileCache('.web_cache'))
geolocator = Nominatim(user_agent="vaccine-spotter-watcher")


@dataclass(eq=True, frozen=True)
class Appointment():
    loc_id: int
    store_id: str
    vaccine_types: Tuple[str]
    provider: str
    url: str
    geo: Tuple[float, float]
    datetime: dt
    city: str
    address: str

    def fullid(self):
        return f"{str(self.loc_id)}:{str(self.datetime.timestamp)}"

    def distance_to(self, home: Location):
        home_geo = (home.latitude, home.longitude)
        return geodesic(home_geo, self.geo).miles


class Filter():
    def __init__(self, seen_appts: Set[Appointment], new_appts: Set[Appointment],
                 distance: int, providers: Set[str], vaccine_types: Set[str],
                 home: Location):
        self.seen_appts = seen_appts
        self.new_appts = new_appts
        self.distance = distance
        self.providers = providers
        self.vaccine_types = vaccine_types
        self.home = home

    def limit(self):
        appts = self.new_appts.difference(self.seen_appts)
        if self.providers:
            appts = filter(lambda x: x.provider in self.providers, appts)
        if self.vaccine_types:
            appts = filter(lambda x: len(set(x.vaccine_types).intersection(self.vaccine_types)) > 0, appts)
        appts = filter(lambda x: x.distance_to(self.home) <= self.distance, appts)
        return set(list(appts))


def state_json(state: str) -> dict:
    resp = sess.get(f'https://www.vaccinespotter.org/api/v0/states/{state}.json')
    return resp.json()


def normalize_address(address: str) -> Location:
    return geolocator.geocode(address, addressdetails=True)


def state_abbr(state: str) -> Optional[str]:
    states = sess.get('https://gist.githubusercontent.com/mshafrir/2646763/raw/8b0dbb93521f5d6889502305335104218454c2bf/states_hash.json')
    states = {v: k for k, v in states.json().items()}
    return states.get(state, None)


def tinyurl(url: str) -> str:
    apiurl = "http://tinyurl.com/api-create.php?url="
    tinyurl = urllib.request.urlopen(apiurl + url).read()
    return tinyurl.decode("utf-8")


def possible_input(slots_data: dict) -> Tuple[set, set]:
    vaccine_types = set()
    providers = set()
    # create possible inputs
    for loc in slots_data['features']:
        if 'appointment_vaccine_types' in loc['properties'] and loc['properties']['appointment_vaccine_types']:
            for vacc_type in loc['properties']['appointment_vaccine_types'].keys():
                vaccine_types.add(vacc_type)
        providers.add(loc['properties']['provider'])
    return (vaccine_types, providers)


def load_seen_appts() -> Set[Appointment]:
    if path.exists(CONFIG_PKL):
        with open(CONFIG_PKL, 'rb') as f:
            return pickle.load(f)
    return set()


def save_seen_appts(appts):
    with open(CONFIG_PKL, 'wb') as f:
        pickle.dump(appts, f)


def load_notifications() -> Dict[str, dt]:
    if path.exists(NOTIFICATION_PKL):
        with open(NOTIFICATION_PKL, 'rb') as f:
            return pickle.load(f)
    return dict()


def save_notifications(notifications):
    with open(NOTIFICATION_PKL, 'wb') as f:
        pickle.dump(notifications, f)


def search(slots_data: dict) -> Set[Appointment]:
    appts = set()
    for loc in slots_data['features']:
        if loc['properties']['appointments_available_all_doses']:
            props = loc['properties']
            lon, lat = loc['geometry']['coordinates']
            store_geo = (lat, lon)
            if len(props['appointments']) > 0:
                for appt in props['appointments']:
                    _appt = Appointment(
                        loc_id=props['id'],
                        store_id=props['provider_location_id'],
                        vaccine_types=tuple(appt.get('vaccine_types', list())),
                        provider=props['provider'],
                        url=props['url'],
                        geo=store_geo,
                        datetime=dt.fromisoformat(appt['time']),
                        city=props['city'],
                        address=props['address']
                    )
                    appts.add(_appt)
            else:
                _appt = Appointment(
                    loc_id=props['id'],
                    store_id=props['provider_location_id'],
                    vaccine_types=tuple(props['appointment_vaccine_types'].keys()),
                    provider=props['provider'],
                    url=props['url'],
                    geo=store_geo,
                    datetime=parser.parse(
                        props['appointments_last_modified']),
                    city=props['city'],
                    address=props['address']
                )
                appts.add(_appt)
    return appts


def notify(appts: Set[Appointment], notifications: Dict[str, dt],
           do_not_notify_timeout_mins: int, phone: str):
    links = dict()
    timeout_limit = dt.now() - timedelta(minutes=do_not_notify_timeout_mins)
    these_notifications = dict()
    providers = dict()
    for appt in appts:
        last_notified = notifications.get(appt.provider, None)
        if last_notified and last_notified > timeout_limit:
            print(
                f'Skipping because we notified for {appt.provider} already - last_notified {last_notified}, timeout: {timeout_limit}')
            continue
        link, count = links.get(appt.provider, ("", 0))
        link = tinyurl(appt.url)
        count += 1
        links[appt.provider] = (link, count)
        these_notifications[appt.provider] = dt.now()
        cities = providers.get(appt.provider, dict())
        addresses = cities.get(appt.city, set())
        addresses.add(appt.address)
        cities[appt.city] = addresses
        providers[appt.provider] = cities
    notifications.update(these_notifications)
    message = []
    for provider, link in links.items():
        cities = providers[provider]
        message_cities = []
        for city, addresses in cities.items():
            message_cities.append(f"{city}: {'; '.join(addresses)}")
        message_cities = '\n\n'.join(message_cities)
        message.append(
            f"🏪 {provider} {link[0]} ({link[1]})\n{message_cities}")
    if message:
        send_message('\n\n'.join(message), phone)


APPLESCRIPT_MESSAGE = """
tell application "Messages"
	set targetBuddy to "+{}"
    set targetService to id of 1st service whose service type = iMessage
    set textMessage to "{}"
    set theBuddy to buddy targetBuddy of service id targetService
    send textMessage to theBuddy
end tell
"""
def send_message(message: str, phone: str):
    command = APPLESCRIPT_MESSAGE.format(phone, message)
    p = Popen(['osascript', '-'], stdin=PIPE, stdout=PIPE,
            stderr=PIPE, universal_newlines=True)
    stdout, stderr = p.communicate(command)
    print("Message attempt (0 = success):", p.returncode, stdout, stderr)


def poll(seen_appts: Set[Appointment], notifications: Dict[str, dt], interval: int,
         distance: int, providers: Set[str], vaccine_types: Set[str], home: Location,
         timeout_last_notice: int, phone_num: str):
    state = state_abbr(home.raw['address']['state'])
    while True:
        try:
            slots_data = state_json(state)
            new_appts = search(slots_data)
            never_seen = new_appts.difference(seen_appts)
            filter = Filter(seen_appts, new_appts, distance,
                            providers, vaccine_types, home)
            notify_appts = filter.limit()
            total_appts = seen_appts | new_appts
            print(f"Appts -- Seen: {len(seen_appts)}, New: {len(never_seen)}, Notified: {len(notify_appts)}, Total: {len(total_appts)}")
            notify(notify_appts, notifications, timeout_last_notice, phone_num)
            save_seen_appts(total_appts)
            save_notifications(notifications)
            seen_appts = total_appts
        finally:
            time.sleep(interval)


def print_options(providers, vaccine_types):
    click.echo(click.style(f"Valid entries for --provider:", fg='blue'))
    click.echo(f"\t{', '.join(providers)}")
    click.echo(click.style(f"Valid entries for --vaccine-type:", fg='blue'))
    click.echo(f"\t{', '.join(vaccine_types)}")
    exit()


def options_check(provider, vaccine_type, providers, vaccine_types):
    error = False
    if provider:
        if not set(provider).issubset(set(providers)):
            click.echo(click.style(
                f"ERROR: --provider should be one or more of:", fg='yellow'))
            click.echo(f"\t{', '.join(providers)}")
            error = True
    if vaccine_type:
        if not set(vaccine_type).issubset(set(vaccine_types)):
            click.echo(click.style(
                f"ERROR: --vaccine-type should be one or more of:", fg='yellow'))
            click.echo(f"\t{', '.join(vaccine_types)}")
            error = True
    if error:
        exit(0)


@click.command()
@click.option('-a', '--address', required=True, type=click.STRING, prompt=True,
    help='Address to use as center for distance search.')
@click.option('-n', '--phone-number', required=True, type=click.STRING, prompt=True,
    help='Phone number to send notifications to.')
@click.option('-d', '--distance', default=20, type=click.INT,
    help='Limit to locations within this distance (miles).')
@click.option('-p', '--provider', multiple=True, default=list(),
    help='Filter for appointments at this provider. Can by supplied multiple times to filter to a list of providers.')
@click.option('-v', '--vaccine-type', multiple=True, default=list(),
    help='Filter for appointments with this vaccine type.  Can by supplied multiple times to filter to a list of vaccine types.')
@click.option('-i', '--interval', default=30, type=click.INT,
    help='Search for new appointments every N seconds.')
@click.option('-t', '--timeout-last-notice', default=15, type=click.INT,
    help='Send one notice per provider every N minutes.')
@click.option('--list-options/--no-list-options', default=False, is_flag=True,
    help='Show the possible filter inputs (provider, vaccine type) given an address and phone number.')
def search_vaccines(address, phone_number, distance, provider, vaccine_type, interval, timeout_last_notice, list_options):
    # setup
    home = normalize_address(address)
    slots_data = state_json(state_abbr(home.raw['address']['state']))
    vaccine_types, providers = possible_input(slots_data)
    if list_options:
        print_options(providers, vaccine_types)
    options_check(provider, vaccine_type, providers, vaccine_types)

    # process
    seen_appts = load_seen_appts()
    notifications = load_notifications()
    poll(seen_appts, notifications, interval, distance, providers,
         vaccine_types, home, timeout_last_notice, phone_number)

if __name__ == "__main__":
    search_vaccines()
